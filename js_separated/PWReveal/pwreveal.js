// Get all password input fields
password_fields = document.querySelectorAll('input[type=password]')
// Turn them into arrays
fields_array = Array.prototype.slice.call(password_fields)

function reveal() {
	fields_array.map(function(el) { el.setAttribute('type', 'text') })
}
function hide() {
	fields_array.map(function(el) {	el.setAttribute('type', 'password') })
}
function reset() {
	fields_array.map(function(el) {	el.value = "" })
}