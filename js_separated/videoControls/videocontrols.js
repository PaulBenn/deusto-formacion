	var video = document.getElementById("video")

	function playOrPause() {
		if (video.paused) {
			video.play()
		} else {
			video.pause()
		}
	}
	function restart() {
		video.currentTime = 0
		video.play()
	}
	function ffOrRew(value) {
		video.currentTime += value
	}