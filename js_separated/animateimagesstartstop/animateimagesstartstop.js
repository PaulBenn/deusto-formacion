var cycler
srcs = [
	"animation_images/b1.png",
	"animation_images/b2.png",
	"animation_images/b3.png",
	"animation_images/b4.png",
	"animation_images/b5.png",
	"animation_images/b6.png",
	"animation_images/b7.png",
	"animation_images/b8.png",
	"animation_images/b9.png",
	"animation_images/b10.png",
	"animation_images/b11.png",
	"animation_images/b12.png"
]
index = 0
stop = false

function start() {
	stop = false
	cycler = setInterval(cycle, 100)
}
function cycle() {
	if(!stop) {
		document.getElementById("animation").src=srcs[index]
		increaseIndex()
	} else {
		clearInterval(cycler)
	}
}
function increaseIndex() {
	if (index === srcs.length - 1) {
		index = 0
	} else {
		index += 1
	}
}
function stopCycle() {
	stop = true
}
window.addEventListener("load", start, false)