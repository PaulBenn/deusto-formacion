green = false

function App() {

    var button = document.getElementById("button")
    var timeout
    // Mouse object
    var mouse = {x: 0, y: 0}

    this.init = function() {

        // Track mouse
        document.addEventListener("mousemove", mouseMove)

        // Initial position
        button.style.transform
            = "translate3d(" + mouse.x + "px, " + mouse.y + "px, 0px)"
    }

    var allowClick = function() {
        button.style.pointerEvents = "all"
    }
    var disableClick = function() {
        button.style.pointerEvents = "none"
    }

    var mouseMove = function(e) {
        // Set local variables
        mouse.x = e.pageX
        mouse.y = e.pageY

        // Don't allow clicking when button is moving
        disableClick()

        button.style.transform
            = "translate3d(" + (mouse.x-100) + "px, " + (mouse.y-50) + "px, 0px)"
        
        // Only allow clicking when button is stationary (25ms after moving)
        clearTimeout(timeout)
        timeout = setTimeout(allowClick, 25)
    }
}

var app = new App()
app.init()

function clicked() {
    if (green) {
        document.body.style.backgroundColor = "red"
        green = false
    } else {
        document.body.style.backgroundColor = "#00cd00"
        green = true
    }
}