var OFFSET = 0
var IMAGE_SIDE = 200
var IMAGE_CENTER_OFFSET = IMAGE_SIDE / 2
var img = new Image();
img.src = "apple.jpg";

function start() {
	element = document.getElementById("canvas")
	canvas = element.getContext("2d")
	setInterval(function () {
		canvas.save()

		canvas.clearRect(0, 0, 500, 500)
		canvas.translate( 250, 250)

		canvas.rotate(OFFSET * Math.PI/180)
		increaseOffset()

		canvas.translate(-IMAGE_CENTER_OFFSET, -IMAGE_CENTER_OFFSET)
		canvas.drawImage(img, 0, 0, IMAGE_SIDE, IMAGE_SIDE)

		canvas.restore()
	}, 10)
}
function increaseOffset() {
	if (OFFSET === 359) {
		OFFSET = 0
	} else {
		OFFSET +=1
	}
}
window.addEventListener("load", start, false)