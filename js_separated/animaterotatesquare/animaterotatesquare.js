var OFFSET = 0
var SQUARE_SIDE = 100
var SQUARE_CENTER_OFFSET = SQUARE_SIDE / 2

function start() {
	element = document.getElementById("canvas")
	canvas = element.getContext("2d")
	setInterval(function () {
		canvas.save()

		canvas.clearRect(0, 0, 500, 500)
		canvas.translate( 250, 250)

		canvas.rotate(OFFSET * Math.PI/180)
		increaseOffset()

		canvas.translate(-SQUARE_CENTER_OFFSET, -SQUARE_CENTER_OFFSET)
		canvas.fillStyle = "#FF0000"
		canvas.fillRect(0, 0, SQUARE_SIDE, SQUARE_SIDE)

		canvas.restore()
	}, 25)
}
function increaseOffset() {
	if (OFFSET === 359) {
		OFFSET = 0
	} else {
		OFFSET +=1
	}
}
window.addEventListener("load", start, false)