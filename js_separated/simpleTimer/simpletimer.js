seconds = 1
document.getElementById("stop").style.display = "none"
document.getElementById("final").style.display = "none"
function start() {
	document.getElementById("stop").style.display = "initial"
	document.getElementById("start").style.display = "none"
	document.getElementById("seconds").innerHTML = seconds
	seconds += 1
	timeoutFunction = setTimeout(start, 1000)
}
function stop() {
	finalTime = seconds - 1
	document.getElementById("seconds").innerHTML = ""
	seconds = 0
	clearTimeout(timeoutFunction)
	document.getElementById("start").style.display = "initial"
	document.getElementById("stop").style.display = "none"
	document.getElementById("final").style.display = "initial"
	document.getElementById("finalTime").innerHTML = finalTime
}