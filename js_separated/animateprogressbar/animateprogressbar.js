var id
var elem = document.getElementById("myBar")
function move() {

    var width = 0
    id = setInterval(frame, 10)
    function frame() {
        if (width >= 100) {
            clearInterval(id)
            elem.style.backgroundColor = "#00cd00"
        } else {
            elem.style.width = width + '%'
            width += 0.1
        }
    }
}
function reset() {
	clearInterval(id)
	elem.style.backgroundColor = "green"
	elem.style.width = 0 + '%'
}