srcs = [
	"animation_images/a1.png",
	"animation_images/a2.png",
	"animation_images/a3.png",
	"animation_images/a4.png",
	"animation_images/a5.png",
	"animation_images/a6.png",
	"animation_images/a7.png",
	"animation_images/a8.png",
	"animation_images/a9.png",
	"animation_images/a10.png",
	"animation_images/a11.png",
	"animation_images/a12.png",
	"animation_images/a13.png"
]
index = 0

function cycle() {
		document.getElementById("animation").src=srcs[index]
		increaseIndex()
}
function increaseIndex() {
	if (index === srcs.length - 1) {
		index = 0
	} else {
		index += 1
	}
}
window.addEventListener("load", cycle, false)

window.onload = function() {
	setInterval(cycle, 100)
}