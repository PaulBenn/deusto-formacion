function check() {
	str = document.getElementById("input").value
	document.getElementById("output-no").innerHTML = str
	if(isPalindrome(format(str))) {
		document.getElementById("output-msg").style.color = "green"
		document.getElementById("output-msg").innerHTML
			= "<b>Yes</b>, you got a palindrome!"
	} else {
		document.getElementById("output-msg").style.color = "red"
		document.getElementById("output-msg").innerHTML
			= "<b>Nope</b>, this isn't a palindrome."
	}
}
function format(str) {
	/* Remove spaces, convert to same case */
	return (str.split(' ').join('')).toLowerCase()
}
function isPalindrome(str) {
	var stop = Math.floor(str.length / 2);
	for (var i = 0; i < stop; i++) {
		if (str[i] !== str[str.length - i - 1])
			return false
	}
	return true;
}
function isPalindrome2(str) {
	/* Also works, but quite a bit slower as it involves array
	modification. Try calling this instead :) */
	return str === str.split('').reverse().join('')
}