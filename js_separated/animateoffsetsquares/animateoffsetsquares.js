// Square side size
var SQUARE_SIDE = 175
// Initial offset
var OFFSET = 0
// Each square's offset
var OFFSET_AMOUNT = 30
// Milliseconds before next square
var SPEED = 1000

index = 0
finish = false

colours = ["#EA0001", "#0000CE", "#00ff00"]

function start() {
	element = document.getElementById("canvas")
	canvas = element.getContext("2d")
	reset()
	createSquares()
}

function reset() {
	finish = false
	index = 0
	OFFSET = 0
}

var createSquares = function() {
	var interval = setInterval(function () {
		if(finish) {
			clearInterval(interval)
		} else {
			if(index >= colours.length) {
				createSquare("#000000")
			} else {
				createSquare(colours[index])
				index += 1
			}
		}
	}, SPEED)
}
var createSquare = function(colour) {
	canvas.fillStyle = colour
	canvas.fillRect(OFFSET, OFFSET, SQUARE_SIDE, SQUARE_SIDE)
	OFFSET += OFFSET_AMOUNT
}
function addToColours() {
	colour = document.getElementById("input").value
	if(isColour(colour)) {
		colours.push(colour)
	}
}
function isColour(str) {
	return (str.length === 7) && (str[0] === "#")
}
function clearCanvas() {
	canvas.clearRect(0, 0, 1000, 1000)
	finish = true
}
