paragraphs = document.getElementsByTagName("p")
buttons = document.getElementsByTagName("button")

function toggleVis(i) {
	if (paragraphs[i].style.visibility === "hidden") {
		paragraphs[i].style.visibility = "visible"
	} else {
		paragraphs[i].style.visibility = "hidden"
	}
	toggleButton(i)
}
function toggleButton(i) {
	if(buttons[i].innerHTML === "Show") {
		buttons[i].innerHTML = "Hide"
	} else {
		buttons[i].innerHTML = "Show"
	}
}