<!-- From Paul - I'm using MySQL Workbench to create and manage a local
    database. Everything on this end seems to work just fine - however,
    XMLHTTPRequests do not work on local servers! Which means I can't test the
    link from HTML to the SQL DB. Theoretically the code below is all you need.
    Here is an example data set to use when opening the connection:

        Server address: localhost
        Server port:    3306
        Username:       root
        Password:       password123
        Schema name:    test
        Table name:     people

    PHP code below is commented - however, just to be clear, here goes a short
    explanation. HTML sends a Q or query value to PHP, with the value from a
    select drop-down list, via an XMLHTTPRequest object. PHP opens a connection
    to a database, runs the query and returns ready-to-use HTML with correct
    information in a table. Note readyState 4 and status code 200 represent
    "request finished/response ready" and "OK" to make sure table is loaded
    only when the information is loaded.

-->

<?php
// GET value from HTML
$q = intval($_GET['q']);
// Open DB connection
$con = mysqli_connect('localhost','root','password','test');
if (!$con) {
    die('Could not connect: ' . mysqli_error($con));
}

// Select schema
mysqli_select_db($con,"test");

// Create query
$sql="SELECT * FROM people WHERE id = '".$q."'";

// Execute query
$result = mysqli_query($con,$sql);

// Begin table inside XML object
echo "<table>"

// Table headers
echo "<tr>
<th>First name</th>
<th>Last name</th>
<th>ID</th>
</tr>";

// Table rows
while($row = mysqli_fetch_array($result)) {
    echo "<tr>";
    echo "<td>" . $row['first_name'] . "</td>";
    echo "<td>" . $row['last_name'] . "</td>";
    echo "<td>" . $row['id'] . "</td>";
    echo "</tr>";
}

// End table inside XML object
echo "</table>";

// Close DB connection
mysqli_close($con);
?>