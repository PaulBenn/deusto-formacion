var debug = false

// UPPERCASE = constants
// lowercase = variables

// Canvas
var CANVAS_WIDTH = 500
var CANVAS_HEIGHT = CANVAS_WIDTH / 2
var FPS = 100
var UPDATE_INTERVAL = 1000 / FPS

// Player
var player;
var PLAYER_HEIGHT = 30
var PLAYER_WIDTH = 30
var PLAYER_COLOUR = "#00CC00"
var PLAYER_COLOUR_CRASH = "#CC0000"
var PLAYER_OFFSET_X = CANVAS_WIDTH / 20 // 5%  of canvas
var PLAYER_OFFSET_Y = CANVAS_HEIGHT / 2 // 50% of canvas

// Platforms
var platforms = [];
var PLATFORM_MIN_WIDTH = PLAYER_WIDTH
var PLATFORM_MAX_WIDTH = 200
var platform_width = PLATFORM_MAX_WIDTH
var PLATFORM_HEIGHT = 8
var PLATFORM_COLOUR = "#000000"
var PLATFORM_MOVE_AMOUNT = 1
var PLATFORM_DECREASE_AMOUNT = 10
var platform_create_interval = 300
var PLATFORM_DECREASE_INTERVAL = 1000 // Every N frames
var PLATFORM_MIN_OFFSET_X = 0;
var platform_max_offset_x = CANVAS_WIDTH - platform_width

function init() {
    player = new component(
    	PLAYER_WIDTH,
    	PLAYER_HEIGHT,
    	PLAYER_COLOUR,
    	PLAYER_OFFSET_X,
    	PLAYER_OFFSET_Y
    	);
    player.gravity = 0.05;
    game.start();
}

var game = {
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.width = CANVAS_WIDTH;
        this.canvas.height = CANVAS_HEIGHT;
        this.context = this.canvas.getContext("2d");
        document.getElementsByTagName('center')[0].appendChild(this.canvas);
        this.frame = 0;
        this.interval = setInterval(updateGameArea, UPDATE_INTERVAL);
        window.addEventListener('keydown', function (e) {
            game.keys = (game.keys || []);
            game.keys[e.keyCode] = true;
        })
        window.addEventListener('keyup', function (e) {
            game.keys[e.keyCode] = false; 
        })
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

function component(width, height, color, x, y, type) {
	this.type = type;
    this.width = width;
    this.height = height;
    this.speedX = 0;
    this.speedY = 0;
    this.x = x;
    this.y = y;
    this.gravity = 0.05;
    this.gravitySpeed = 0;
    this.update = function() {
        ctx = game.context;
      	ctx.fillStyle = color;
      	ctx.fillRect(this.x, this.y, this.width, this.height);
    }
    this.newPos = function() {
    	this.gravitySpeed += this.gravity;
        this.x += this.speedX;
        this.y += this.speedY + this.gravitySpeed;
        this.hitSides()
    }
    this.hitSides = function() {
        var bottom = CANVAS_HEIGHT - this.height;
        if (this.y > bottom) {
            this.y = bottom;
            this.gravitySpeed = 0;
        }
        var top = 0;
        if (this.y < top) {
            this.y = top;
            this.gravitySpeed = 0;
        }
        var left = 0;
        if (this.x < left) {
            this.x = left;
            this.gravitySpeed = 0;
        }
        var right = CANVAS_WIDTH - this.width;
        if (this.x > right) {
            this.x = right;
            this.gravitySpeed = 0;
        }
    }
    this.crashesWith = function(platform) {
        var playerleft = this.x;
        var playerright = this.x + (this.width);
        var playertop = this.y;
        var playerbottom = this.y + (this.height);
        var platformleft = platform.x;
        var platformright = platform.x + (platform.width);
        var platformtop = platform.y;
        var platformbottom = platform.y + (platform.height);
        return !((playerbottom < platformtop)
            || (playertop > platformbottom)
            || (playerright < platformleft)
            || (playerleft > platformright));

    }
    this.crash = function() {
    	ctx.fillStyle = PLAYER_COLOUR_CRASH
    	ctx.fillRect(this.x, this.y, PLAYER_WIDTH, PLAYER_HEIGHT)
	}
}

function updateGameArea() {
	// Detect collision
    for (i = 0; i < platforms.length; i += 1) {
        if (player.crashesWith(platforms[i])) {
        	player.crash()
            return;
        }
    }
    game.clear();
    game.frame += 1;
    if (game.frame === 1 || everyinterval(platform_create_interval)) {
	    var offset = randomNumberBetween(PLATFORM_MIN_OFFSET_X,platform_max_offset_x)
        console.log(offset)
        if(!debug) {
        	platforms.push(new component(
        	platform_width,
        	PLATFORM_HEIGHT,
        	PLATFORM_COLOUR,
        	offset,
        	0));
        }
    }
    for (i = 0; i < platforms.length; i += 1) {
        platforms[i].y += PLATFORM_MOVE_AMOUNT;
        platforms[i].update();
    }

    updateScore(game.frame)
    updateDiff(game.frame)

    // Keyboard input
    player.speedX = 0;
    player.speedY = 0;
    if (game.keys && game.keys[32]) { jump() } // Space
    if (game.keys && game.keys[37]) { player.speedX = -2; } // Left
    //if (game.keys && game.keys[38]) { player.speedY = -1; } // Up
    if (game.keys && game.keys[39]) { player.speedX = 2; } // Right
    //if (game.keys && game.keys[40]) { player.speedY = 1; } // Down
    // End keyboard input

    player.newPos();
    player.update();
}

function everyinterval(n) {
    return (game.frame / n) % 1 === 0;

}

function accelerate(n) {
    player.gravity = n;
}

function updateScore(n) {
	document.getElementById("score").innerHTML = n
}
function updateDiff(n) {
	if (n % PLATFORM_DECREASE_INTERVAL === 0) {
		if (platform_width <= PLATFORM_MIN_WIDTH) {
			platform_width = PLATFORM_MIN_WIDTH
		} else {
			platform_width -= PLATFORM_DECREASE_AMOUNT
		}
	}
}

function jump() {
	player.gravity = -0.3
	sleep(100).then(() => {
		player.gravity = 0.1
	})
}

function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

function randomNumberBetween(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

function moveup() {
    player.speedY -= 1; 
}

function movedown() {
    player.speedY += 1; 
}

function moveleft() {
    player.speedX -= 1;
}

function moveright() {
    player.speedX += 1;
}
function stop() {
    player.speedX = 0;
    player.speedY = 0; 
}